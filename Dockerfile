FROM golang

WORKDIR /go/src/gitlab.com/alexanderinsa_projects/public/authentication-service

COPY . .

RUN go install -v .

EXPOSE 8080

CMD ["authentication-service"]

