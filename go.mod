module gitlab.com/alexanderinsa_projects/public/authentication-service

go 1.15

require (
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.9.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/yaml.v2 v2.2.8
)
