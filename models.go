package main

var sampleUser = user{
	ID:       1,
	Username: "username",
	Password: "password",
}

type user struct {
	ID       uint64 `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type User struct {
	UserName  string
	Email     string
	FirstName string
	LastName  string
}