package main

import (
	// "database/sql"
	"io/ioutil"
	"log"

	"github.com/gin-gonic/gin"
	JWTMiddleWare "github.com/appleboy/gin-jwt/v2"
	"golang.org/x/crypto/bcrypt"
)

func Authenticate(c *gin.Context) (interface{}, error) {
	var loginVals login
	if err := c.ShouldBind(&loginVals); err != nil {
		return "", JWTMiddleWare.ErrMissingLoginValues
	}
	userID := loginVals.Username
	password := loginVals.Password

	userPassword := DB.QueryRow("select password from users where username=$1", userID)
	
	var dbUser login
	if err := userPassword.Scan(&dbUser.Password); err != nil {
		log.Fatal(err)
		return nil, JWTMiddleWare.ErrFailedAuthentication
	}
	log.Println("Successfully returned results")

	if err := bcrypt.CompareHashAndPassword([]byte(dbUser.Password), []byte(password)); err != nil {
		log.Println(err)
		return nil, JWTMiddleWare.ErrFailedAuthentication
	}
	log.Println("Password successful")

	userInformation := DB.QueryRow("select username, email, firstname, lastname from users where username=$1", userID)

	var returnUser User
	if err := userInformation.Scan(&returnUser.UserName, &returnUser.Email, &returnUser.FirstName, &returnUser.LastName,); err != nil {
		log.Println(err)
		return nil, JWTMiddleWare.ErrFailedAuthentication
	}
	return returnUser, nil
}

func Authorize(data interface{}, c *gin.Context) bool {
	if v, ok := data.(*User); ok && v.UserName == "admin" {
		return true
	}

	return false
}

func getKey(path string) []byte {
	signBytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal("Unable to read private key at: ", path)
		log.Fatal(err)
		panic(err)
	}
	return signBytes
}