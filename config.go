package main

import (
	"log"
	"os"
	
	envconfig "github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

type Config struct {
    Server struct {
        Host string `yaml:"host" envconfig:"SERVER_HOST"`
        Port string `yaml:"port" envconfig:"SERVER_PORT"`
    } `yaml:"server"`
    Database struct {
		Host	 string `yaml:"host" envconfig:"DB_HOST"`
		Port     string `yaml:"port" envconfig:"DB_PORT"`
		Database     string `yaml:"database" envconfig:"DB_DATABASE"`
        Username string `yaml:"username" envconfig:"DB_USERNAME"`
        Password string `yaml:"password" envconfig:"DB_PASSWORD"`
        SSLMode string `yaml:"sslmode" envconfig:"DB_SSLMODE"`
    } `yaml:"database"`
    RSAKeySet struct {
        PrivateKeyPath string `yaml:"privateKeyPath" envconfig:"PRIVATE_KEY_PATH"`
        PublicKeyPath string `yaml:"publicKeyPath" envconfig:"PUBLIC_KEY_PATH"`
    } `yaml:"rsa"`
}


func readFile(config *Config) {
	file, err := os.Open("config.yml")
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
	defer file.Close()

	decoder := yaml.NewDecoder(file)
	err = decoder.Decode(config)
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}

func readEnv(config *Config) {
	err := envconfig.Process("", config)
    if err != nil {
		log.Fatal(err)
		panic(err)
    }
}