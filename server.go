package main

import (
	"database/sql"
	"log"
	"strings"
	"time"


	JWTMiddleWare "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var (
	ApplicationConfig Config
	DB *sql.DB
)

func main() {
	readFile(&ApplicationConfig)
	readEnv(&ApplicationConfig)
	router := setupRouter()
	DB = initDB()
	
	log.Fatal(router.Run(ApplicationConfig.Server.Host + ":" + ApplicationConfig.Server.Port))
}

func initDB() *sql.DB {
	databaseStringProperties := []string{
		"dbname=",
		ApplicationConfig.Database.Database,
		" user=",
		ApplicationConfig.Database.Username,
		" password=",
		ApplicationConfig.Database.Password,
		" host=",
		ApplicationConfig.Database.Host,
		" port=",
		ApplicationConfig.Database.Port,
		" sslmode=",
		ApplicationConfig.Database.SSLMode,
	}
	var databaseString strings.Builder
	for _, s := range databaseStringProperties {
		databaseString.WriteString(s)
	}
	db, err := sql.Open("postgres", databaseString.String())
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
	return db
}

func setupRouter() *gin.Engine {
	router := gin.Default()

	// config := cors.DefaultConfig()
	// config.AllowOrigins = []string{"http://localhost:3000"}
	// router.Use(cors.New(config))
	router.Use(cors.Default())

	authMiddleware, err := JWTMiddleWare.New(&JWTMiddleWare.GinJWTMiddleware{
		Realm: 				"amorgenstern",
		Key:				getKey(ApplicationConfig.RSAKeySet.PrivateKeyPath),
		PrivKeyFile:		ApplicationConfig.RSAKeySet.PrivateKeyPath,
		PubKeyFile:			ApplicationConfig.RSAKeySet.PublicKeyPath,
		SigningAlgorithm: 	"RS256",
		SendCookie:			true,
		CookieDomain:		"localhost",
		SecureCookie:       false,
		CookieHTTPOnly:		false,
		Timeout:			time.Hour,
		MaxRefresh:			time.Hour,
		IdentityKey: 		"id",
		Authenticator: 		Authenticate,
		Authorizator: 		Authorize,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}
	router.POST("/login", authMiddleware.LoginHandler)

	auth := router.Group("/auth")

	auth.GET("/refresh_token", authMiddleware.RefreshHandler)
	auth.Use(authMiddleware.MiddlewareFunc())
	{
		// auth.GET("/hello", helloHandler)
	}
	// authorized := router.Group("/auth", gin.BasicAuth(gin.Accounts{
	// 	"foo": "bar",
	// }))s
	// authorized.POST("refresh_token", refresh)
	return router
	
}